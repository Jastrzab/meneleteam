-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 15 Sty 2020, 13:43
-- Wersja serwera: 10.1.36-MariaDB
-- Wersja PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sklep1`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `adres`
--

CREATE TABLE `adres` (
  `id` int(10) NOT NULL,
  `ulica` text COLLATE utf8_polish_ci NOT NULL,
  `kod_pocztowy` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `telefon` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `kraj` text COLLATE utf8_polish_ci NOT NULL,
  `id_klient` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `adres`
--

INSERT INTO `adres` (`id`, `ulica`, `kod_pocztowy`, `telefon`, `kraj`, `id_klient`) VALUES
(5, 'Różana', '54-500', '90623567', 'Polska', 1),
(6, 'Wojska Polskiego', '54-500', '682057296', 'Niemcy', 1),
(9, 'Belwederska', '58-270', '592650378', 'Polska', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategoria`
--

CREATE TABLE `kategoria` (
  `id` int(10) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `kategoria`
--

INSERT INTO `kategoria` (`id`, `nazwa`) VALUES
(1, 'Nabiał'),
(2, 'Mięso'),
(3, 'Pieczywo'),
(4, 'Przyprawy'),
(5, 'Alkohol'),
(6, 'Napoje');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id` int(10) NOT NULL,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`id`, `imie`, `nazwisko`) VALUES
(1, 'Genowefa', 'Moherek'),
(4, 'Michał', 'Dąbrowski');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `koszyk`
--

CREATE TABLE `koszyk` (
  `id` int(10) NOT NULL,
  `id_adres` int(10) NOT NULL,
  `produkty` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `koszyk`
--

INSERT INTO `koszyk` (`id`, `id_adres`, `produkty`) VALUES
(3, 9, 'Koszyk1'),
(4, 5, 'Koszyk2'),
(5, 6, 'Koszyk3'),
(6, 9, 'Koszyk4');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(10) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `id_kategoria` int(11) NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`id`, `nazwa`, `opis`, `id_kategoria`, `cena`) VALUES
(1, 'Jogurt', 'Jogurt naturalny', 1, 1),
(2, 'Piwo', 'Piwo 4,5%', 5, 3),
(3, 'Chleb', 'Chleb zwykły krojony', 3, 2),
(4, 'Pierś kurzęcia', 'Mięso drobiowe', 2, 13),
(7, 'Pieprz', 'Pieprz mielony czarny', 4, 1),
(8, 'Sól', 'Sól morska ', 4, 1),
(9, 'Sok', 'Sok pomarańczowy', 6, 3),
(10, 'Woda', 'Woda mineralna', 6, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamownienia`
--

CREATE TABLE `zamownienia` (
  `id` int(10) NOT NULL,
  `id_koszyk` int(10) NOT NULL,
  `koszt_dostawy` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zamownienia`
--

INSERT INTO `zamownienia` (`id`, `id_koszyk`, `koszt_dostawy`) VALUES
(1, 3, 15),
(2, 4, 10),
(3, 5, 12),
(4, 6, 16);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `adres`
--
ALTER TABLE `adres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_klient` (`id_klient`);

--
-- Indeksy dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `koszyk`
--
ALTER TABLE `koszyk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_adres` (`id_adres`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategoria` (`id_kategoria`);

--
-- Indeksy dla tabeli `zamownienia`
--
ALTER TABLE `zamownienia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_koszyk` (`id_koszyk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `adres`
--
ALTER TABLE `adres`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `koszyk`
--
ALTER TABLE `koszyk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `zamownienia`
--
ALTER TABLE `zamownienia`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `adres`
--
ALTER TABLE `adres`
  ADD CONSTRAINT `adres_ibfk_1` FOREIGN KEY (`id_klient`) REFERENCES `klient` (`id`);

--
-- Ograniczenia dla tabeli `koszyk`
--
ALTER TABLE `koszyk`
  ADD CONSTRAINT `koszyk_ibfk_1` FOREIGN KEY (`id_adres`) REFERENCES `adres` (`id`);

--
-- Ograniczenia dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD CONSTRAINT `produkt_ibfk_1` FOREIGN KEY (`id_kategoria`) REFERENCES `kategoria` (`id`);

--
-- Ograniczenia dla tabeli `zamownienia`
--
ALTER TABLE `zamownienia`
  ADD CONSTRAINT `zamownienia_ibfk_1` FOREIGN KEY (`id_koszyk`) REFERENCES `koszyk` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
