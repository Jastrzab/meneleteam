-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 23 Paź 2019, 08:21
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `yet`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `ID` int(11) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `adres` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `telefon` varchar(32) COLLATE utf8mb4_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`ID`, `nazwa`, `adres`, `telefon`, `NIP`) VALUES
(1, 'ety', '54t', '34', '34t');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `ID` int(10) NOT NULL,
  `alias` varchar(128) COLLATE utf8mb4_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `miasto` varchar(128) COLLATE utf8mb4_polish_ci NOT NULL,
  `kod` varchar(16) COLLATE utf8mb4_polish_ci NOT NULL,
  `kraj` varchar(128) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`ID`, `alias`, `ulica`, `miasto`, `kod`, `kraj`) VALUES
(1, 'magazyn_m', 'szeroka', 'Szczecin', '78-255', 'Polska'),
(2, 'magazyn_d', 'Taka', 'Warszawa', '78-888', 'Polska'),
(3, 'magazyn_ten', 'Jedna', 'Lublin', '45-666', 'Polska'),
(4, 'magazyn_BPI', 'Lipowa', 'Lipiany', '74-240', 'Polska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `ID` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `specyfikacja` text COLLATE utf8mb4_polish_ci DEFAULT NULL,
  `kod_produktu` varchar(16) COLLATE utf8mb4_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`ID`, `nazwa`, `specyfikacja`, `kod_produktu`, `cena`) VALUES
(1, 'opel corsa', 'hfghgf', 'fhfg', 123),
(2, 'opel astra', 'szybki', '213', 321);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID` int(10) NOT NULL,
  `ilość` int(10) NOT NULL,
  `IDproduktu` int(10) NOT NULL,
  `IDmag` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID`, `ilość`, `IDproduktu`, `IDmag`) VALUES
(1, 0, 1, 1),
(2, 2, 1, 4);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDproduktu` (`IDproduktu`),
  ADD KEY `IDmag` (`IDmag`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
