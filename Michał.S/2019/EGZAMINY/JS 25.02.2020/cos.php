<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tabela</title>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
    <div class="tak">
        Treść:<br>
        <textarea name="text" class="tresc" id="text" cols="40" rows="10"></textarea>
        <br><button onclick="start()">Prześlij</button>
        <br><h1>Lista komentarzy:</h1>
        <div class="wynik"></div>
    </div>
    <script type="text/javascript">
    function start() {
        //alert("cos")
        var tresc = document.querySelector('.tresc');
        document.querySelector(".wynik").innerHTML+= "<div class='komentarz'>"+tresc.value+"<span class='usun' onclick='usun(this)'>Usun</span></div>";
        tresc.innerHTML = '';
        tresc.value = '';
    }
    function usun(element){
        var getObiekt = element.parentElement;
        //alert(getObiekt);
        getObiekt.remove();
        //alert("test");
    }
    </script>
</body>
</html>