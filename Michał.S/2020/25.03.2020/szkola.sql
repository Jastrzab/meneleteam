-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 30 Mar 2020, 22:20
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `szkoła`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasa`
--

CREATE TABLE `klasa` (
  `id` int(11) NOT NULL DEFAULT 0,
  `nazwa` varchar(2) CHARACTER SET utf8 NOT NULL,
  `il_uczniow` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klasa`
--

INSERT INTO `klasa` (`id`, `nazwa`, `il_uczniow`) VALUES
(1, '1a', 20),
(2, '1b', 30),
(3, '2a', 40),
(4, '2b', 50);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczen`
--

CREATE TABLE `uczen` (
  `id` int(2) NOT NULL DEFAULT 0,
  `nazwisko` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `imie` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `miejsce_urodzenia` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `id_klasy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uczen`
--

INSERT INTO `uczen` (`id`, `nazwisko`, `imie`, `miejsce_urodzenia`, `id_klasy`) VALUES
(1, 'Kybica', 'Maciek', 'Wrocław', 1),
(2, 'Zawadzki', 'Zbigniew', 'Wrocław', 1),
(3, 'Rudy', 'Antoni', 'Wrocław', 2),
(4, 'Kowalski', 'Marcin', 'Wrocław', 3),
(5, 'Dawid', 'Ozdoba', 'Niemampojecia', 2),
(6, 'Kaczmarek', 'Robert', 'Wrocław', 4),
(7, 'Kowalski', 'Jan', 'Wrocław', 4),
(8, 'Polak', 'Janusz', 'Sosnowiec', 2),
(9, 'Piotr', 'Paweł', 'Wrocław', 3),
(10, 'Góral', 'Łukasz', 'Trzebnica', 4),
(11, 'Nowak', 'Jan', 'Oleśnica', 4),
(12, 'Kowalski', 'Łukasz', 'Wrocław', 1),
(13, 'Mostowiak', 'Karton', 'Wrocław', 3),
(14, 'Baryła', 'Zenon', 'Oława', 2),
(15, 'Gota', 'Anna', 'Oleśnica', 4),
(16, 'Kowalska', 'Justyna', 'Wrocław', 1),
(17, 'Długopis', 'Magda', 'Wrocław', 3),
(18, 'Szary', 'Tomasz', 'Trzebnica', 1),
(19, 'Bury', 'Łukasz', 'Oława', 3),
(20, 'Rudy', 'Wojciech', 'Wrocław', 2),
(21, 'Kowalska', 'Janina', 'Oława', 2),
(22, 'Nowak', 'Jan', 'Wrocław', 1),
(23, 'Kowalski', 'Riko', 'Wrocław', 3),
(24, 'Nowakowska', 'Natalia', 'Wrocław', 1),
(25, 'Lojalna', 'Jolanta', 'Wrocław', 2),
(26, 'Konarski', 'Krzysztof', 'Oława', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wychowawca`
--

CREATE TABLE `wychowawca` (
  `id` int(11) NOT NULL DEFAULT 0,
  `imie` varchar(20) CHARACTER SET utf8 NOT NULL,
  `nazwisko` varchar(30) CHARACTER SET utf8 NOT NULL,
  `id_klasy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `wychowawca`
--

INSERT INTO `wychowawca` (`id`, `imie`, `nazwisko`, `id_klasy`) VALUES
(1, 'Jan', 'Janusz', 1),
(2, 'Michał', 'Kubica', 2),
(3, 'Jolanta', 'Nielojalna', 3),
(4, 'Andrzej', 'Kaczyński', 4),
(5, 'Robert', 'Górski', 5);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `klasa`
--
ALTER TABLE `klasa`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uczen`
--
ALTER TABLE `uczen`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `wychowawca`
--
ALTER TABLE `wychowawca`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
